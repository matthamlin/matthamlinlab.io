# Components

## Active




## Inactive

### Header:

```HTML
<header class="clearfix mb2 white secondary-background-color">
    <div class="left">
        <a href="#!" class="btn py2 m0 caps">CampusLife</a>
    </div>
    <div class="clearfix sm-hide"></div>
        <div class="overflow-hidden px2 py1">
            <button class="right btn btn-primary" type="button" id="search-button">Find</button>
            <input type="text" class="right fit field primary-color quaternary-background-color" placeholder="Search All">
        </div>
    </div>
</header>
```
